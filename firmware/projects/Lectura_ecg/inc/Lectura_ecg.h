/*! @mainpage Lectura de ECG
 *
 * \section genDesc General Description
 * Use of ADC and DAC to retrieve ECG input, can apply low-pass filter
 * with initial frequency limit set as 40Hz. This can be varied at 5Hz at a time.
 *
 * <a href= "https://drive.google.com/file/d/1CuAWfEvsIGDBXI3XhfUPIVUmhNadhxYN/view?usp=sharing" >Operation Example (video)</a>
 *
 ** \section Switch Function
 *
 * |   Switch 1		|   		Function		 |
 * |:--------------:|:---------------------------|
 * | 	SWITCH_1 	|  	Turns on filter			 |
 * | 	SWITCH_2 	|  	Turns off filter		 |
 * | 	SWITCH_3 	|	Lowers frequency limit	 |
 * | 	SWITCH_4	| 	Elevates frequency limit |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 10/10/2020	| Project Started			                     |
 * | 22/10/2020	| Project Finished			                     |
 *
 * @author Bruno Maximiliano Breggia
 *
 */


#ifndef LECTURA_ECG_H
#define LECTURA_ECG_H


/*================================[inclusions]===============================*/

#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"


/*===========================[Definitions and Constants]======================*/

#define BUFFER_SIZE 231

const char ecg[BUFFER_SIZE] = {
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

/*==================[external functions declaration]=========================*/

int main(void);

/*
 * @brief Inicializa hardware
 */
void Sysinit(void);

/*
 * @brief Pasa a un valor de tension el proximo
 * 		  elemento numerico del vector con el ECG
 */
void WriteECG(void);

/*
 * @brief Actuliza medicion del valor de tension
 */
void ConversionAD(void);

/*
 * @brief Fija frecuencia de muestreo
 * @param Nueva frecuencia de corte (en kHz)
 */
void fijarFrecuenciaDeCorte(float frec);

/*
 * @brief Evia dato leido por conversor-AD
 * 		  por puerto serie
 */
void EnviarDatoSerie(void);

/*
 * @brief Aplica filtro pasa-bajos al ultimo dato leido
 */
void filtrarDato(void);

/*
 * @brief Activa aplicacion del filtro
 */
void FiltroOn(void);

/*
 * @brief Desactiva aplicacion del filtro
 */
void FiltroOff(void);

/*
 * @brief Aumenta en 5 Hz la frecuencia de corte
 */
void SubirCorte(void);

/*
 * @brief Disminuye en 5 Hz la frecuencia de corte
 */
void BajarCorte(void);

/*==================[end of file]============================================*/


#endif /* #ifndef LECTURA_ECG_H */

