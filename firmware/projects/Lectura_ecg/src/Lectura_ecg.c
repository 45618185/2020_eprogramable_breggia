/**
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor: Bruno M. Breggia
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*================================[inclusions]===============================*/
#include "../inc/Lectura_ecg.h"       /* <= own header */

/*===========================[Definitions and Constants]======================*/

// Muestreo
#define CHANNEL CH1
#define PERIODO	2 // ms
#define MODE 	AINPUTS_SINGLE_READ

// Accesorios
#define TIMER_READ   TIMER_A	// Trabaja en ms
#define TIMER2_WRITE TIMER_B	// Trabaja en ms
#define PUERTO   SERIAL_PORT_PC
#define BAUDIOS  115200

// Constantes matematicas
#define PI 3.14159

uint16_t dato = 0, datoAnterior = 0;
uint16_t count = 0;
float alfa = 0;
float f_corte = 0; // frecuencia de corte en Hz
bool filtrar = false;

timer_config TimerData;
serial_config UartData;
analog_input_config ConversorData;

/*==========================[Function Definitions]============================*/

void WriteECG(void) {
	AnalogOutputWrite(ecg[count]);
	if (++count == BUFFER_SIZE) count = 0;
}

void ConversionAD(void) {
	datoAnterior = dato;
	AnalogInputRead(CHANNEL, &dato);
	if ( filtrar ) filtrarDato();
	EnviarDatoSerie();
}

void EnviarDatoSerie(void) {
	UartSendString(PUERTO, UartItoa(dato, 10) );
	UartSendString(PUERTO, (uint8_t*)"\r");
}

void fijarFrecuenciaDeCorte(float frec) {
	f_corte = frec;
	alfa = 2*PI*frec*PERIODO/1000; // paso intermedio
	alfa /= (alfa + 1);
}

void filtrarDato(void) {
	dato = datoAnterior + alfa*(dato - datoAnterior);
}

void FiltroOn(void) {
	filtrar = true;
	LedOn(LED_RGB_B);
}

void FiltroOff(void) {
	filtrar = false;
	LedOff(LED_RGB_B);
}

void SubirCorte(void) {
	if (f_corte < 80) fijarFrecuenciaDeCorte(f_corte + 5);
}

void BajarCorte(void) {
	if (f_corte > 5) fijarFrecuenciaDeCorte(f_corte - 5);
}

/*============================[System Initializer]============================*/

void SysInit(void) {
	// General configurations
	SystemClockInit();
	LedsInit();
	SwitchesInit();

	// Analog Output Configuration
	AnalogOutputInit();

	// Analog Input Configuration
	ConversorData = (analog_input_config) {CHANNEL, MODE, ConversionAD};
	fijarFrecuenciaDeCorte(40);
	AnalogInputInit(&ConversorData);

	// UART configuration
	UartData = (serial_config) {PUERTO, BAUDIOS, NULL};
	UartInit(&UartData);

	// Timer read Configuration
	TimerData = (timer_config) {TIMER_READ, PERIODO, AnalogStartConvertion};
	TimerInit(&TimerData);
	TimerStart(TIMER_READ);

	// Timer 2 Configuration
	TimerData = (timer_config) {TIMER2_WRITE, 2*PERIODO, WriteECG};
	TimerInit(&TimerData);
	TimerStart(TIMER2_WRITE);

	// Interruption configurations
	SwitchActivInt(SWITCH_1, FiltroOn);
	SwitchActivInt(SWITCH_2, FiltroOff);
	SwitchActivInt(SWITCH_3, BajarCorte);
	SwitchActivInt(SWITCH_4, SubirCorte);
}

/*===================================[Main]===================================*/

int main(void)
{
	SysInit();

	while(1);

	return 0;
}

/*===============================[end of file]================================*/

