/*! @mainpage Medidor de Distancia con Timer
 *
 * \section genDesc General Description
 * Application that sensors distance, each 10cm a new led turns on.
 * Switches can hold value or turn off measurement, uses interruptions.
 * The measurements are updated by using the microcontroller's timer
 *
 * <a href="https://drive.google.com/file/d/1EmXce1HudC1GWfWJ2vBN5xMx8mrOvBou/view?usp=sharing">Operation Example (video)</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	|  	T_FIL2		|
 * | 	TRIGGER 	|  	T_FIL3		|
 * | 	GROUND	 	| 	GND			|
 * | 	Vcc	 		| 	5V			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 23/09/2020	| Project creation			                     |
 * | 09/10/2020	| Project finished			                     |
 *
 * @author Bruno Maximiliano Breggia
 *
 */


#ifndef _MEDIDOR_DISTANCIA_H
#define _MEDIDOR_DISTANCIA_H


/*================================[inclusions]===============================*/

#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"


/*==================[external functions declaration]=========================*/

int main(void);

/*!
 * @brief Turns off all indicator leds
 * @return TRUE is nothing went wrong
 */
bool turnOffLeds(void);

/*!
 * @brief Turns on leds according to measurement:
 * |   medicionCm		|   Led turned on	|
 * |:------------------:|:------------------|
 * | 	0 to 10	 		|  	LED_RGB_B		|
 * | 	10 to 20 		|  	+ LED_1			|
 * | 	20 to 30 		| 	+ LED_2			|
 * | 	>30	 			| 	+ LED_3			|
 * @param medicionCm: the measurement, in cm
 * @return TRUE is nothing went wrong
 */
bool mostrarMedicion(void);

/*!
 * \brief Changes state of Active flag
 */
void ToggleActivo(void);

/*!
 * @brief Changes state of Hold flag
 */
void ToggleHold(void);

/*!
 * @brief Refreshes led display according to distance measured
 */
void ActualizarMedicion(void);

/*
 * @brief Sends measurement as ASCII characters by serial port
 */
void EnviarMedicionSerie(void);

/**
 * @brief Interruption function called when there is serial input
 */
void RecibirEntradaSerie(void);

/*!
 * @brief Initializes microcontroller and device drivers,
 * and sets the Timer and interruptions.
 */
void SysInit(void);

/*==================[end of file]============================================*/


#endif /* #ifndef _MEDIDOR_DISTANCIA_H */

