/**
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor: Bruno M. Breggia
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*================================[inclusions]===============================*/
#include "../inc/medidor_distancia_timer.h"       /* <= own header */


/*===========================[Definitions and Constants]========================*/

// Hardware connection logistic
#define ECHO    GPIO_T_FIL2
#define TRIGGER GPIO_T_FIL3

// Timer management
#define TIMER   TIMER_B			// con milliseconds. Timer A lo usa el Delay!!
#define UPDATE_MILLISECS 1000 	// 1 sec

// Serial port configuration
#define PUERTO  SERIAL_PORT_PC
#define BAUDIOS 115200

/*!
 * @brief Global variables for ACTIVE and HOLD state.
 */
bool activo = 1, hold = 0;

/**
 * @brief Global variable for storing updated measurement
 */
int16_t medicionCm = 0;

timer_config TimerData;
serial_config UartData;

/*==========================[Function Definitions]============================*/

bool turnOffLeds(void) {
	LedOff(LED_RGB_B);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
	return true;
}

bool mostrarMedicion(void) {
	turnOffLeds();
	if (medicionCm >  0) LedOn(LED_RGB_B);
	if (medicionCm > 10) LedOn(LED_1);
	if (medicionCm > 20) LedOn(LED_2);
	if (medicionCm > 30) LedOn(LED_3);
	return true;
}

void ActualizarMedicion(void) {
	if ( activo ){
		if ( !hold ) {
			medicionCm = HcSr04ReadDistanceCentimeters();
			mostrarMedicion();
			EnviarMedicionSerie();
		}
	} else turnOffLeds();
}

void EnviarMedicionSerie(void) {
	UartSendString(PUERTO, UartItoa(medicionCm, 10) );
	UartSendString(PUERTO, (uint8_t*)" cm\r\n");
}

void RecibirEntradaSerie(void) {
	uint8_t dato;
	UartReadByte(PUERTO, &dato);
	switch(dato) {
	case 'H': ToggleHold();
		break;
	case 'O': ToggleActivo();
		break;
	}
}

void ToggleActivo(void) {
	activo = !activo;
	// Si se prende, continua funcionando sin hold
	if (activo) hold = 0;
}

void ToggleHold(void) {
	hold = !hold;
}


/*============================[System Initializer]============================*/

void SysInit(void) {
	// General configurations
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	HcSr04Init(ECHO, TRIGGER);

	// UART configuration
	UartData = (serial_config) {PUERTO, BAUDIOS, RecibirEntradaSerie};
	UartInit(&UartData);

	// Timer Configuration
	TimerData = (timer_config) {TIMER, UPDATE_MILLISECS, ActualizarMedicion};
	TimerInit(&TimerData);
	TimerStart(TIMER);

	// Interruption configurations
	SwitchActivInt(SWITCH_1, ToggleActivo);
	SwitchActivInt(SWITCH_2, ToggleHold);
}



/*===================================[Main]===================================*/

int main(void)
{
	SysInit();

	while(1);

	HcSr04Deinit(ECHO, TRIGGER);

	return 0;
}

/*===============================[end of file]===============================*/

