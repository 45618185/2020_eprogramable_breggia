/*! @mainpage Ejercicio 1
 *
 * \section genDesc General Description
 *
 * This application senses distance and blinks a LED
 * at increasing frequency with object proximity
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	|  	T_FIL2		|
 * | 	TRIGGER 	|  	T_FIL3		|
 * | 	GROUND	 	| 	GND			|
 * | 	Vcc	 		| 	5V			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/11/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Bruno Breggia
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*================================[inclusions]===============================*/

#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"


/*==================[external functions declaration]=========================*/

int main(void);

/**
 * @fn void SysInit(void)
 * @brief Inicializacion de hardware
 */
void SysInit(void);

/**
 * @fn void ActualizarMedicion(void)
 * @brief Realiza nueva medicion de distancia en cm
 */
void ActualizarMedicion(void);

/**
 * @fn void adaptarFrecuencia(void)
 * @brief Calcula nuevo valor de frecuencia segun
 * la ultima medicion de distancia
 */
void adaptarFrecuencia(void);

/**
 * @fn void toggleLed(void)
 * @brief Conmuta el estado de LED1 (on/off)
 */
void toggleLed(void);


/*==================[cplusplus]==============================================*/

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

