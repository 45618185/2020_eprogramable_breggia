﻿Descripción del Proyecto: goniometro digital de rodilla

Se emplea un potenciometro solidario al eje de dos piezas moviles.
La señal de tension analogica va de 0V para 0 grados de apertura, 
hasta 3.3V para 180 grados. Envia por puerto serie el ultimo valor
medido y el maximo que se registro hasta el momento.
