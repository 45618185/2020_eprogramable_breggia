/**
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor: Bruno M. Breggia
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */




/*==================[inclusions]=============================================*/
#include "../inc/ejercicio2.h"       /* <= own header */

/*==================[macros and definitions]=================================*/

// Hardware logistic
#define VOUT_PIN GPIO_T_FIL3
#define VMAX 	 3.3
#define CHANNEL CH1

// Timer management
#define TIMER_MEDICION   TIMER_A
#define PERIODO_MUESTREO 2 // ms

// Serial communication
#define PUERTO  SERIAL_PORT_PC
#define BAUDIOS 115200
#define MODE 	AINPUTS_SINGLE_READ

bool active = false;   /**< Global variable for remembering program state (on/off) */
uint16_t datoCrudo = 0;    /**< Global variable for storing raw input measurement */
uint16_t lecturaAngulo = 0;	/**< Global variable for storing angle measurement [°] */
int16_t maxVal = 0; /**< Global variable for storing maximum measurement [°] */

// Estructuras de inicializacion
timer_config TimerData;
serial_config UartData;
analog_input_config ConversorData;

/*==========================[function declarations]=========================*/

void ActualizarMedicion(void) {
	if (!active) return;
	AnalogInputRead(CHANNEL, &datoCrudo);
	lecturaAngulo = pasar_a_Grados(datoCrudo);
	if (lecturaAngulo > maxVal) maxVal = lecturaAngulo;
	EnviarDatoSerie();
}

uint16_t pasar_a_Grados(uint16_t lectura) {
	return (lectura / 1023) * 180;
}

void EnviarDatoSerie(void) {
	UartSendString(PUERTO, UartItoa(lecturaAngulo, 10) );
	UartSendString(PUERTO, (uint8_t*)"-");
	UartSendString(PUERTO, UartItoa(maxVal, 10) );
	UartSendString(PUERTO, (uint8_t*)"\r\n");
}

void encender(void) {
	active = true;
	GPIOOn(VOUT_PIN);
}

void apagar(void) {
	active = false;
	GPIOOff(VOUT_PIN);
}

void resetear(void) {
	maxVal = 0;
}

void SysInit(void) {
	// General configurations
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	GPIOInit(VOUT_PIN, GPIO_OUTPUT);
	GPIOOff(VOUT_PIN);

	// Analog Input Configuration
	ConversorData = (analog_input_config) {CHANNEL, MODE, ActualizarMedicion};
	AnalogInputInit(&ConversorData);

	// UART configuration
	UartData = (serial_config) {PUERTO, BAUDIOS, NULL};
	UartInit(&UartData);

	// Timer Configuration
	TimerData = (timer_config) {TIMER_MEDICION, PERIODO_MUESTREO, AnalogStartConvertion};
	TimerInit(&TimerData);
	TimerStart(TIMER_MEDICION);

	// Interruption configurations
	SwitchActivInt(SWITCH_1, encender);
	SwitchActivInt(SWITCH_2, apagar);
	SwitchActivInt(SWITCH_3, resetear);

}


/*==================================[main]==================================*/

int main(void){

	SysInit();

	while(1){

	}

	return 0;
}

/*==================[end of file]============================================*/

