/*! @mainpage Ejercicio 2
 *
 * \section genDesc General Description
 *
 * Goniometro digital de rodilla
 *
 * \section hardConn Conexion del potenciometro
 *
 * |  Potenciometro	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	Vcc 	 	|  	T_FIL3		|
 * | 	GROUND	 	| 	GND			|
 * | 	Vout 		| 	ADC			|
 *
 * \section switchConf Configuracion de switches
 *
 * |   Device 1		|   Funcion		|
 * |:--------------:|:--------------|
 * | 	Switch1	 	|  	enciende	|
 * | 	Switch2	 	| 	apaga		|
 * | 	Switch3		| 	resetea		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 09/11/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Bruno Breggia
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*================================[inclusions]===============================*/

#include "led.h"
#include "switch.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include "analog_io.h"
#include "gpio.h"


/*==================[external functions declaration]=========================*/

int main(void);

/**
 * @fn void SysInit(void)
 * @brief Inicializacion de hardware
 */
void SysInit(void);

/**
 * @fn void ActualizarMedicion(void)
 * @brief Realiza nueva medicion de tension analogica
 * 		  a la salida del potenciometro
 */
void ActualizarMedicion(void);

/**
 * @fn uint16_t pasar_a_Grados(uint16_t lectura)
 * @brief Conversion del dato leido por el ADC en un angulo de 0 a 180.
 * @param valor de lectura del ADC
 * @return valor en grados sexagesimales (0-180)
 */
uint16_t pasar_a_Grados(uint16_t lectura);

/**
 * @fn void enviarDatoSerie(void)
 * @brief Envia por puerto serie ultima medicion de angulo leido y
 * 		  maximo valor registrado hasta el momento
 */
void EnviarDatoSerie(void);

/**
 * @fn void encender(void)
 * @brief Da inicio a la medicion analogica
 */
void encender(void);

/**
 * @fn void apagar(void)
 * @brief Pone en pausa la medicion analogica
 */
void apagar(void);

/**
 * @fn void resetear(void)
 * @brief Vuelve a cero el maximo valor registrado hasta el momento
 */
void resetear(void);


/*==================[cplusplus]==============================================*/

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

