/*
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor:
 * Bruno M. Breggia
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*================================[inclusions]===============================*/
#include "medidor_distancia.h"       /* <= own header */


/*===========================[Definitions and Constants]========================*/

#define ECHO    GPIO_T_FIL2
#define TRIGGER GPIO_T_FIL3
#define BIT0 0
#define BIT1 1
#define ON 1
#define OFF 0

const uint8_t mascara0 = 1 << BIT0;
const uint8_t mascara1 = 1 << BIT1;

/// \brief Global variable for storing ECU-CIAA switch-lecture intput
uint8_t teclas = 0;


/*==========================[Function Definitions]============================*/

bool turnOffLeds(void) {
	LedOff(LED_RGB_B);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
	return true;
}

bool mostrarMedicion(uint8_t medicionCm) {
	turnOffLeds();
	if (medicionCm >  0) LedOn(LED_RGB_B);
	if (medicionCm > 10) LedOn(LED_1);
	if (medicionCm > 20) LedOn(LED_2);
	if (medicionCm > 30) LedOn(LED_3);
	return true;
}

uint8_t DetectarFlancoAscendente(enum SWITCHES switchID, uint8_t bandera) {
	if (teclas == switchID){
		if ( (bandera & mascara0) == OFF ) {
			if ((bandera & mascara1) == mascara1)
				bandera |= mascara0;
		} else if ( (bandera & mascara1) == 0 )
			bandera &= ~mascara0;
	} else {
		if ((bandera & mascara0) == OFF) bandera |= mascara1;
		else bandera &= ~mascara1;
	}
	return bandera;
}

bool isOn(uint8_t bandera){
	return (bandera & mascara0) == ON;
}

bool isOff(uint8_t bandera){
	return (bandera & mascara0) == OFF;
}


/*===================================[Main]===================================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	HcSr04Init(ECHO, TRIGGER);

	uint8_t medicion_cm = 0;

	/**
	 * Banderas de estado - se usan solo los ultimos dos bits:
	 * * El bit LSB es indicador del estado actual
	 * * El 2do bit LSB indica cuando corresponde negar el bit LSB (hace de bit de memoria)
	 * Esto evita un toggle intermitente mientras se mantiene
	 * presionada la tecla responsable de su cambio de estado
	 */
	uint8_t activo = 1;
	uint8_t hold = 0;

	while(1){
		teclas = SwitchesRead();

		// Detecto cambios de estado en estado ACTIVO
		activo = DetectarFlancoAscendente(SWITCH_1, activo);
		// Detecto cambios de estado en estado HOLD
		hold = DetectarFlancoAscendente(SWITCH_2, hold);

		// Realizo medicion si esta activo
		if ( isOn(activo) ){
			if ( isOff(hold) ) {
				medicion_cm = HcSr04ReadDistanceCentimeters();
				mostrarMedicion(medicion_cm);
			}
		} else turnOffLeds();
	}

	HcSr04Deinit(ECHO, TRIGGER);

	return 0;
}

/*===============================[end of file]===============================*/

