/*! @mainpage Template
 *
 * \section genDesc General Description
 *
 * This section describes how the program works.
 *
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	 PWM	 	| 	T_FIL1		|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 06/11/2020 | Document creation		                         |
 * | 			| 	                     						 |
 *
 * @author Bruno Breggia
 *
 */

#ifndef SERVOFUNC_H
#define SERVOFUNC_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

#include "systemclock.h"
#include "gpio.h"
#include "bool.h"
#include "led.h"
#include "servo.h"
#include "delay.h"

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef SERVOFUNC_H */

