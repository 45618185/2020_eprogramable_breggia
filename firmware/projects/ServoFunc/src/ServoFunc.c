/*! @mainpage Blinking
 *
 * \section genDesc General Description
 *
 * This application makes a servo move
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 01/01/2020 | Document creation		                         |
 * |			|							                     |
 *
 * @author Bruno Breggia
 *
 */

/*===============================[inclusions]================================*/
#include "../inc/ServoFunc.h"       /* <= own header */

/*============================[macros and definitions]=======================*/
#define N_SERVOS   1

/*======================[internal data definition]===========================*/
servo_t miServo = SERVO0;

/*==================[internal functions declaration]=========================*/



/*==================[external functions definition]==========================*/

int main(void){
	SystemClockInit();
	LedsInit();
	ServoInit(&miServo, N_SERVOS);

    while(1) {
    	ServoAngle(miServo, 0);
    	DelaySec(3);
    	ServoAngle(miServo, 90);
    	DelaySec(3);
    	ServoAngle(miServo, 179);
    	DelaySec(3);
    	ServoAngle(miServo, 90);
    	DelaySec(3);
    }

	ServoDeinit();

	return 0;
}

/*==================[end of file]============================================*/

