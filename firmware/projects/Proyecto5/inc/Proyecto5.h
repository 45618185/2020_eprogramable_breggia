/*! @mainpage Sensor de Luz para un invernadero
 *
 * \section genDesc General Description
 * Sensor de luz diseñado para invernaderos pequeños. En caso
 * de detectar demasiada luz, activa servo para cubrir las plantas con un
 * techo. En caso de ser muy oscuro, prende luces. Utiliza un filtro pasa-bajos
 * para mejorar la calidad de la señal y evitar triggers inadecuados.
 *
 * <a href="https://drive.google.com/file/d/1K5gDHmql_6JhB79PywPikIIoIM3aMc08/view?usp=sharing">Ejemplo de funcionamiento(video)</a>
 *
 * \section hardConn Hardware Connection
 *
 * |    Funcion 		|   EDU-CIAA	|
 * |:------------------:|:--------------|
 * | 	Voltaje sensor	|  	T_FIL3		|
 * | 	Lectura analog 	|  	CH1 		|
 * | 	PWM  		 	| 	T_FIL2		|
 * | 	Voltage leds	| 	T_FIL0		|
 * | 	Vcc servo		| 	5V			|
 *
 *
 * \section switchConfig Switch Configuration
 *
 * |   Switch 1		|   Accion		|
 * |:--------------:|:--------------|
 * | 	Switch1	 	|  	hold/unhold	|
 * | 	Switch2 	|  	filtrar		|
 * | 	Switch3	 	|aumento f_corte|
 * | 	Switch4		|reduzco f_corte|
 *
 * @author Bruno Maximiliano Breggia
 *
 */

#ifndef _PROYECTO5_H
#define _PROYECTO5_H


/*==================[inclusions]=============================================*/

#include "switch.h"
#include "systemclock.h"
#include "gpio.h"
#include "bool.h"
#include "led.h"
#include "string.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "servo.h"

/*============================[function declaration]=========================*/

int main(void);

/**
 * @fn void SysInit(void)
 * @brief Iniciliza los componentes del sistema
 */
void SysInit(void);

/**
 * @fn void readVolt(void)
 * @brief Realiza la lectura analogica del sensor de luz y efectua
 * 		  los analisis correspondientes de la señal
 */
void readVolt(void);

/**
 * @fn void verifico_sobrexposicion(void)
 * @brief Realiza las comparaciones necesarias para verificar si
 * 		  es necesario colocar/remover techo al invernadero en
 * 		  base a las mediciones del sensor
 */
void verifico_sobrexposicion(void);

/**
 * @fn void verifico_oscuridad(void)
 * @brief Realiza las comparaciones necesarias para verificar si
 * 		  es necesario prender/apagar luces en el invernadero en
 * 		  base a las mediciones del sensor
 */
void verifico_oscuridad(void);

/**
 * @fn void PasaBajos(void)
 * @brief Aplica filtro pasa bajos a la ultima medicion
 * 		  realizada por el sensor.
 */
void PasaBajos(void);

/**
 * @fn void EnviarDatoSerie(void)
 * @brief Envia por puerto serie la lectura del sensor
 */
void EnviarDatoSerie(void);

/**
 * @fn void AccionarTecho(void)
 * @brief Activa el servo para colocar el techo sobre
 * 		  el invernadero, y prende un led indicador.
 */
void AccionarTecho(void);

/**
 * @fn void RemoverTecho(void)
 * @brief Activa el servo para quitar el techo sobre
 * 		  el invernadero, y apaga led indicador.
 */
void RemoverTecho(void);

/**
 * @fn void fijarFrecuenciaDeCorte(float frec)
 * @brief Fija la frecuencia de corte del pasabajos
 * @param frec: nueva frecuencia de corte a emplear
 */
void fijarFrecuenciaDeCorte(float frec);

/**
 * @fn void RecibirEntradaSerie(void)
 * @brief Determina la accion a realizar ante una entrada por
 * 		  el puerto serie. Las entradas que espera recibir son:
 * |   entrada serie	|   accion a realizar				|
 * |:------------------:|:----------------------------------|
 * | 	' '		 		|  	toggle_hold						|
 * | 	'a'				|  	aumenta en 2 el umbral sup		|
 * | 	'z'				| 	disminuye en 2 el umbral sup	|
 * | 	'o'				| 	aumenta en 2 el umbral inf		|
 * | 	'l'				| 	disminuye en 2 el umbral inf	|
 */
void RecibirEntradaSerie(void);

/**
 * @fn void cambiarUmbralInferior(int8_t delta)
 * @brief Suma al umbral de ilumnacion inferior el valor
 * 		  entero que se le suministre (positivo o negativo).
 * @param delta: monto a incrementar (positivo) o decrementar (negativo)
 */
void cambiarUmbralInferior(int8_t delta);

/**
 * @fn void cambiarUmbralSuperior(int8_t delta)
 * @brief Suma al umbral de ilumnacion superior el valor
 * 		  entero que se le suministre (positivo o negativo).
 * @param delta: monto a incrementar (positivo) o decrementar (negativo)
 */
void cambiarUmbralSuperior(int8_t delta);

/**
 * @fn void toggle_filtro(void)
 * @brief Conmuta el estado de la bandera de filtrado.
 * 		  Si se encuentra en true, habilita el pasabajos.
 */
void toggle_filtro(void);

/**
 * @fn void SubirCorte(void)
 * @brief Aumenta la frecuencia de corte del pasabajos en Hz
 */
void SubirCorte(void);

/**
 * @fn void BajarCorte(void)
 * @brief Reduce la frecuencia de corte del pasabajos en 5Hz
 */
void BajarCorte(void);

/*==================[end of file]============================================*/

#endif /* #ifndef _PROYECTO5_H */

