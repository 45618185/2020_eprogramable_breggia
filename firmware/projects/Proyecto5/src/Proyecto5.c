/**
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor: Bruno M. Breggia
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==============================[inclusions]=================================*/

#include "../inc/Proyecto5.h"

/*==========================[macros and definitions]=========================*/

// Componentes fisicos
#define PIN1 GPIO_T_FIL3
#define PIN2 GPIO_T_FIL0
#define LED1 LED_RGB_B
#define LED2 LED_1
#define LED3 LED_2

// Constantes I/O
#define CHANNEL CH1
#define PERIODO	5 // ms
#define MODE 	AINPUTS_SINGLE_READ

// Accesorios
#define TIMER_READ TIMER_A // Trabaja en ms
#define PUERTO     SERIAL_PORT_PC
#define BAUDIOS    115200
#define N_SERVOS   1 // Cantidad de servos a utilizar

// Indices de arreglo
#define ULTIMO_DATO 0
#define DATO_PREVIO 1

// Lectura de datos
uint16_t datos[2] = {0,0}; /**< Los dos ultimos datos leidos */

// Variables para comparacion
uint16_t umbralInf = 50;  /**< Umbral que determina cuando prender las luces  */
uint16_t umbralSup = 80;  /**< Umbral que determina cuando activar sombra     */
uint16_t contadorInf = 0; /**< Contador de tolerancia antes de prender luces  */
uint16_t contadorSup = 0; /**< Contador de tolerancia antes de activar sombra */

// Banderas
bool superaUmbralInf = true;  /**< Bandera indicativa de que se supero umbralInf */
bool superaUmbralSup = false; /**< Bandera indicativa de que se supero umbralSup */
bool filtrar = false; /**< Bandera indicativa de que el filtro esta activo    */
bool hold = true;     /**< Bandera indicativa de que el sistema esta inactivo */

// Parametros variables
float alfa=0;    /**< Coeficiente para el pasa-bajos (depende de f_corte) */
float f_corte=0; /**< Frecuencia de corte del pasa-bajos [Hz] */
uint8_t angulo = 0;         /**< Angulo que se hara girar el servo [grados sexagesimales] */
uint16_t tolerancia = 3000; /**< Margen de tiempo para considerar que se paso por el umbral [ms] */

// Estructuras de inicializacion
timer_config  TimerData;
serial_config UartData;
analog_input_config ConversorData;
servo_t miServo = SERVO0;

/*=====================[internal functions declaration]======================*/

void readVolt(void) {
	if (hold) return;

	// medicion
	datos[DATO_PREVIO] = datos[ULTIMO_DATO];
	GPIOOn(PIN1);
	AnalogInputRead(CHANNEL, datos);
	GPIOOff(PIN1);
	datos[ULTIMO_DATO] = 100 - datos[ULTIMO_DATO]; // paso a porcentaje

	// filtrado
	if (filtrar) PasaBajos();

	// comparacion limite inferior
	verifico_sobrexposicion();

	// comparacion limite superior
	verifico_oscuridad();

	// envio de datos
	EnviarDatoSerie();
}

void verifico_sobrexposicion(void) {
	// Verifico si pasa umbral superior
	if (datos[ULTIMO_DATO] > umbralSup &&
			contadorSup < tolerancia/PERIODO) {
		++contadorSup;
		if (contadorSup == tolerancia/PERIODO) AccionarTecho();
	}

	// Verifico si vuelve a valor normal
	if (datos[ULTIMO_DATO] < umbralSup &&
			contadorSup > 0) {
		--contadorSup;
		if (contadorSup == 0) RemoverTecho();
	}
}

void verifico_oscuridad(void) {
	// Verifico si pasa umbral inferior
	if (datos[ULTIMO_DATO] < umbralInf &&
			contadorInf < tolerancia/PERIODO) {
		++contadorInf;
		if (contadorInf == tolerancia/PERIODO) GPIOOn(PIN2);
	}

	// Verifico si vuelve a valor normal
	if (datos[ULTIMO_DATO] > umbralInf &&
			contadorInf > 0) {
		--contadorInf;
		if (contadorInf == 0) GPIOOff(PIN2);
	}
}

void EnviarDatoSerie(void) {
	UartSendString(PUERTO, UartItoa((datos[ULTIMO_DATO]), 10) );
	UartSendString(PUERTO, (uint8_t*)"\r");
}

void AccionarTecho(void) {
	angulo = 179;
	ServoAngle(miServo, angulo);
	LedOn(LED3);
}

void RemoverTecho(void) {
	angulo = 0;
	ServoAngle(miServo, angulo);
	LedOff(LED3);
}

void PasaBajos(void) {
	datos[ULTIMO_DATO] = datos[DATO_PREVIO] +
			alfa*(datos[ULTIMO_DATO] - datos[DATO_PREVIO]);
}

void toggle_filtro(void) {
	filtrar = !filtrar;
	if (filtrar) LedOn(LED2);
	else LedOff(LED2);
}

void toggle_hold(void) {
	hold = !hold;
	if (!hold) LedOn(LED1);
	else LedOff(LED1);
}

void fijarFrecuenciaDeCorte(float frec) {
	f_corte = frec;
	alfa = 2*3.14*frec*PERIODO/1000; // paso intermedio
	alfa /= (alfa + 1);
}

void SubirCorte(void) {
	if (f_corte < 80) fijarFrecuenciaDeCorte(f_corte + 5);
}

void BajarCorte(void) {
	if (f_corte >  5) fijarFrecuenciaDeCorte(f_corte - 5);
}

void cambiarUmbralInferior(int8_t delta) {
	if ((umbralInf+delta) > 0 && (umbralInf+delta) < umbralSup)
		umbralInf += delta;
}

void cambiarUmbralSuperior(int8_t delta) {
	if ((umbralSup+delta) > umbralInf && (umbralSup+delta) < 100)
		umbralInf += delta;
}

void RecibirEntradaSerie(void) {
	uint8_t dato;
	UartReadByte(PUERTO, &dato);
	switch(dato) {
	case ' ': toggle_hold();
		break;
	case 'a': cambiarUmbralSuperior( 2);
		break;
	case 'z': cambiarUmbralSuperior(-2);
		break;
	case 'o': cambiarUmbralInferior( 2);
		break;
	case 'l': cambiarUmbralInferior(-2);
		break;
	}
}

void SysInit(void) {
	// Configuraciones elementales
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	GPIOInit(PIN1, GPIO_OUTPUT);
	GPIOInit(PIN2, GPIO_OUTPUT);

	// Configuracion de entrada analogica
	ConversorData = (analog_input_config) {CHANNEL, MODE, readVolt};
	AnalogInputInit(&ConversorData);
	fijarFrecuenciaDeCorte(20);

	// Configuracion de la UART
	UartData = (serial_config) {PUERTO, BAUDIOS, RecibirEntradaSerie};
	UartInit(&UartData);

	// Configuracion Servo
	ServoInit(&miServo, N_SERVOS);
	angulo = 0;
	ServoAngle(miServo, angulo);

	// Configuration del timer
	TimerData = (timer_config) {TIMER_READ, PERIODO, AnalogStartConvertion};
	TimerInit(&TimerData);
	TimerStart(TIMER_READ);

	// Configuracion de interrupciones
	SwitchActivInt(SWITCH_1, toggle_hold);
	SwitchActivInt(SWITCH_2, toggle_filtro);
	SwitchActivInt(SWITCH_3, SubirCorte);
	SwitchActivInt(SWITCH_4, BajarCorte);
}

/*=======================[main function definition]==========================*/

int main(void){

	SysInit();

	while(1) { }

	return 0;
}

/*================================[end of file]==============================*/

