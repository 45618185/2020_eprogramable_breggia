﻿Descripción del Proyecto:
El presente proyecto consiste en el sensado de luz con la EDU-CIAA, para un invernadero
pequeño de techo removible. En caso de detectar demasiada exposición, se accionará un servo
para la colocación de un techo. En caso de detectar demasiada oscuridad, se prenderán luces
auxiliares.
Para activar los componentes anteriores se utilizan umbrales de sensado que pueden ajustarse
mediante entrada de datos por puerto serie desde la PC. La señal analógica leída por el micro
puede ser sometida a un filtrado pasa-bajos, para quitar ruido que pueda ocasionar activación
inadecuada de los accesorios de este proyecto. La señal se envía a la PC por puerto serie.
