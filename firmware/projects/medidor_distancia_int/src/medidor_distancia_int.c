/**
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor: Bruno M. Breggia
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*================================[inclusions]===============================*/
#include "../inc/medidor_distancia_int.h"       /* <= own header */


/*===========================[Definitions and Constants]========================*/

#define ECHO    GPIO_T_FIL2
#define TRIGGER GPIO_T_FIL3

/*!
 * @brief Global variables for ACTIVE and HOLD state.
 * Last bit indicates present state, second to last bit indicates
 * when the LSB should toggle. This avoids constant toggling when
 * switch is kept pressed.
 */
uint8_t activo = 1, hold = 0;


/*==========================[Function Definitions]============================*/

bool turnOffLeds(void) {
	LedOff(LED_RGB_B);
	LedOff(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
	return true;
}

bool mostrarMedicion(uint8_t medicionCm) {
	turnOffLeds();
	if (medicionCm >  0) LedOn(LED_RGB_B);
	if (medicionCm > 10) LedOn(LED_1);
	if (medicionCm > 20) LedOn(LED_2);
	if (medicionCm > 30) LedOn(LED_3);
	return true;
}


/*============================[Interruptions]================================*/

void ISR_Tecla1(uint8_t teclas) {
	activo = !activo;
}

void ISR_Tecla2(uint8_t teclas) {
	hold = !hold;
}


/*===================================[Main]===================================*/

int main(void)
{
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	HcSr04Init(ECHO, TRIGGER);

	uint8_t medicion_cm = 0;

	SwitchActivInt(SWITCH_1, ISR_Tecla1);
	SwitchActivInt(SWITCH_2, ISR_Tecla2);

	while(1){

		if ( activo ){
			if ( !hold ) {
				medicion_cm = HcSr04ReadDistanceCentimeters();
				mostrarMedicion(medicion_cm);
			}
		} else turnOffLeds();
	}

	HcSr04Deinit(ECHO, TRIGGER);

	return 0;
}

/*===============================[end of file]===============================*/

