/*! @mainpage Medidor de Distancia con Interrupciones
 *
 * \section genDesc General Description
 * Application that sensors distance, each 10cm a new led turns on.
 * Switches can hold value or turn off measurement, uses interruptions.
 *
 * <a href="https://drive.google.com/file/d/1ebBov3VnMYBqh1KkKXBwpcWQhVyvtfym/view?usp=sharing">Operation Example (video)</a>
 *
 * \section hardConn Hardware Connection
 *
 * |   Device 1		|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	|  	T_FIL2		|
 * | 	TRIGGER 	|  	T_FIL3		|
 * | 	GROUND	 	| 	GND			|
 * | 	Vcc	 		| 	5V			|
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 22/09/2020 | Document creation		                         |
 * | 21/09/2020	| Project finished  							 |
 * | 03/09/2020	| Project creation			                     |
 *
 * @author Bruno Maximiliano Breggia
 *
 */


#ifndef _MEDIDOR_DISTANCIA_H
#define _MEDIDOR_DISTANCIA_H


/*================================[inclusions]===============================*/

#include "hc_sr4.h"
#include "led.h"
#include "switch.h"
#include "systemclock.h"


/*==================[external functions declaration]=========================*/

int main(void);

/*!
 * @brief Turns off all indicator leds
 * @return TRUE is nothing went wrong
 */
bool turnOffLeds(void);

/*!
 * @brief Turns on leds according to measurement:
 * |   medicionCm		|   Led turned on	|
 * |:------------------:|:------------------|
 * | 	0 to 10	 		|  	LED_RGB_B		|
 * | 	10 to 20 		|  	+ LED_1			|
 * | 	20 to 30 		| 	+ LED_2			|
 * | 	>30	 			| 	+ LED_3			|
 * @param medicionCm: the measurement, in cm
 * @return TRUE is nothing went wrong
 */
bool mostrarMedicion(uint8_t medicionCm);

/*!
 * \brief Detects if last bit of a flag variable is 1.
 * \param bandera (8-bit flag variable)
 * \return TRUE if last bit of bandera is 1
 */
void ISR_Tecla1(uint8_t teclas);

/*!
 * @brief Detects if last bit of a flag variable is 0.
 * @param bandera (8-bit flag variable)
 * @return TRUE if last bit of bandera is 0
 */
void ISR_Tecla2(uint8_t teclas);




/*==================[end of file]============================================*/


#endif /* #ifndef _MEDIDOR_DISTANCIA_H */

