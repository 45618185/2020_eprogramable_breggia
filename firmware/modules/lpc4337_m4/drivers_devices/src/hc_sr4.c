/**
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor:
 * Bruno M. Breggia
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* \brief Bare Metal header for leds on EDU-CIAA NXP
 *
 */

/*
 * Initials     Name
 * ---------------------------
 *	BMB		Bruno Maximiliano Breggia
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200921 v2.1 BMB
 * 20200918 v2.0 BMB
 * 20200902 v1.0 BMB
 */


/*===============================[Inclusions]================================*/
#include "hc_sr4.h"       /* <= own header */


/*===========================[Global Variables]===============================*/

/**
 * \brief Global variables that will keep the pin id info
 * for the trigger output and echo input
 */
uint8_t trigger_pin, echo_pin;


/*===============================[Functions]================================*/

bool HcSr04Init(gpio_t echo, gpio_t trigger){
	// Save pins into global variables
	echo_pin = echo;
	trigger_pin = trigger;

	// Initialize pins
	GPIOInit(echo_pin, GPIO_INPUT);
	GPIOInit(trigger_pin, GPIO_OUTPUT);

	// Set trigger pin off for precaution
	GPIOOff(trigger_pin);
	return true;
}

int16_t HcSr04ReadDistanceCentimeters(void) {
	// El manual pide dividir por 58, pero por experiencia
	// en el debugueo, me arroja una distancia equivalente a
	// la mitad a la que se encuentra realmente el objeto.
	return HcSr04GetTimeMicroseconds() / 29;
}

int16_t HcSr04ReadDistanceInches(void) {
	// El manual pide dividir por 148, pero por experiencia
	// en el debugueo, me arroja una distancia equivalente a
	// la mitad a la que se encuentra realmente el objeto.
	return HcSr04GetTimeMicroseconds() / 74;
}

int16_t HcSr04GetTimeMicroseconds(void) {
	uint16_t microsecs = 0;

	GPIOOn(trigger_pin);
	DelayUs(10);
	GPIOOff(trigger_pin);

	// Wait until echo signal arrives
	while(GPIORead(echo_pin)==0);

	// Count time of echo signal
	while(GPIORead(echo_pin)==1) {
		DelayUs(1);
		microsecs += 1;
	}

	return microsecs;
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger) {
	GPIODeinit();
	return true;
}

/*==================[end of file]============================================*/

