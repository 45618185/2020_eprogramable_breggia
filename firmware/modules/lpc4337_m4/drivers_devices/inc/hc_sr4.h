/**
 * Copyright 2020,
 * Bruno Maximiliano Breggia
 * breggiabruno@gmail.com
 * Facultad de Ingenieria
 * Universidad Nacional de Entre Rios
 * Argentina
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _HC_SR4_H
#define _HC_SR4_H

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Devices Drivers devices
 ** @{ */
/** \addtogroup HCSR04 Ultrasonic sensor
 ** @{ */

/** @brief Bare Metal header for leds on EDU-CIAA NXP
 **
 ** This is a driver for an external ultrasonic sensor Hc_Sr04
 **
 **/

/*
 * Initials     Name
 * ---------------------------
 *	BMB			Bruno Maximiliano Breggia
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20200921 v1.5 BMB
 * 20200902 v1.0 BMB
 */



/*===============================[Inclusions]================================*/

#include <stdint.h>
#include "gpio.h"
#include "bool.h"
#include "delay.h"


/*==========================[Function declarations]==========================*/


/** @fn bool HcSr04Init(gpio_t echo, gpio_t trigger)
 * @brief Initialization of echo input pin and trigger output pin
 * @param[in] echo pin, trigger pin
 * @return TRUE if no error occurs
 */
bool HcSr04Init(gpio_t echo, gpio_t trigger);

/** @fn int16_t HcSr04GetTimeMicroseconds(void)
 * @brief Executes the sensoring process and counts the time it takes to receive the echo signal back
 * @return Time in microseconds
 */
int16_t HcSr04GetTimeMicroseconds(void);

/** @fn int16_t HcSr04ReadDistanceCentimeters(void)
 * @brief Returns the distance measured in centimeters
 * @return Distance in centimeters
 */
int16_t HcSr04ReadDistanceCentimeters(void);

/** @fn int16_t HcSr04ReadDistanceInches(void)
 * @brief Returns the distance measured in inches
 * @return Distance in inches
 */
int16_t HcSr04ReadDistanceInches(void);

/** @fn bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
 * @brief De-initializes gpio pins
 * @param[in] echo pin, trigger pin
 * @return TRUE if no error occurs
 */
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

#endif /* #ifndef _HC_SR4_H */

