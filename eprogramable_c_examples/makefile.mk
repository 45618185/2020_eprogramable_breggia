########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe


#########################################################################
# EJERCICIOS

#PROYECTO_ACTIVO = Ejercicio_1_2_3
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_4_5_6
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_7_8_9
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_10_11_12
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_13
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_14
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_16
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_17_18
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_A
#NOMBRE_EJECUTABLE = Ejer.exe

#PROYECTO_ACTIVO = Ejercicio_C
#NOMBRE_EJECUTABLE = Ejer.exe

PROYECTO_ACTIVO = Ejercicio_D
NOMBRE_EJECUTABLE = Ejer.exe