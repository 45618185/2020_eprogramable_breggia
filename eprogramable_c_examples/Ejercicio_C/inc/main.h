
#ifndef _MAIN_H
#define _MAIN_H
/*==================[inclusions]=============================================*/
#include <stdint.h>


/*==================[macros and definitions]=================================*/
uint8_t BinaryToBcd(uint32_t data, uint8_t digits, uint8_t *bcd_number);
int main(void);



/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /*  */

