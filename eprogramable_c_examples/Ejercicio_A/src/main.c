/*
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Bruno Breggia
 *
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

typedef enum {ON=0, OFF, TOGGLE} MODE;
typedef enum {LED_1=1, LED_2, LED_3} NroLed;

typedef struct {
	uint8_t n_led; 		// indica el número de led a controlar
	uint8_t n_ciclos; 	// indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo; 	// indica el tiempo de cada ciclo
	uint8_t mode; 		// ON, OFF, TOGGLE
} my_leds;

void manejadorDeLeds( my_leds* leds ) {

	uint8_t i = 0;
	uint8_t j;

	switch(leds->mode){
	case ON:

		switch (leds->n_led){
		case LED_1:
			printf("Prendo LED 1\n");
			break;
		case LED_2:
			printf("Prendo LED 2\n");
			break;
		case LED_3:
			printf("Prendo LED 3\n");
			break;
		}

		break;
		case OFF:

			switch (leds->n_led){
			case LED_1:
				printf("Apago LED 1\n");
				break;
			case LED_2:
				printf("Apago LED 2\n");
				break;
			case LED_3:
				printf("Apago LED 3\n");
				break;
			}

			break;
	case TOGGLE:


		while(i<leds->n_ciclos) {

			switch (leds->n_led){
			case LED_1:
				printf("Conmuto LED 1\n");
				break;
			case LED_2:
				printf("Conmuto LED 2\n");
				break;
			case LED_3:
				printf("Conmuto LED 3\n");
				break;
			}
			++i;
			j=0;
			while (j< leds->periodo)
				++j;

		}

		break;
	default:
		return;
	}
}

int main(void)
{

	my_leds a = {LED_2, 10, 100, TOGGLE};
	my_leds *ptr_a = &a;

	manejadorDeLeds(ptr_a);

	return 0;
}

/*==================[end of file]============================================*/

