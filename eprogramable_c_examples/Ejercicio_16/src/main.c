/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 *
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

int main(void)
{
	// Ejercicio 16:
	//				 a) Declare una variable sin signo de 32 bits y carge el valor de 0x01020304.
	//					Declare cuatro variables sin signo de 8 bits y, utilizando máscaras,
	//					rotaciones y truncamiento, cargue cada uno de los bytes de la variable de 32 bits.
	//				 b) Realice el mismo ejercicio, utilizando la definición de una “union”.

	uint32_t num = 0x01020304;
	uint8_t a,b,c,d;

	// Desempaquetado de num usando mascaras
	a = num;
	b = num>>8;
	c = num>>16;
	d = num>>24;

	printf("a= %d \nb= %d \nc= %d \nd= %d \n", a, b, c, d);

	// Desempaquetado de num usando "union"
	union {
		uint32_t numero;
		uint8_t partes[4];
	} empaquetado;

	empaquetado.numero = num;
	uint8_t i;
	for(i=0; i<4; ++i) {
		printf("Porcion %d = %d \n", i, empaquetado.partes[i]);
	}

	return 0;
}

/*==================[end of file]============================================*/

