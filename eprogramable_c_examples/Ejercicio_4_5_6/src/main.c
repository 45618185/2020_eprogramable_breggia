/*
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Bruno M. Breggia
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>

/*==================[macros and definitions]=================================*/
#define BIT0 0
#define BIT2 2
#define BIT4 4

/*==================[internal functions declaration]=========================*/

int main(void)
{
	// Ejer 4: Declare una variable de 32 bits sin signo, con el valor inicial 0x0000
	//		   y luego, mediante una operación de máscaras, coloque a 1 el bit 2.
    uint32_t a = 0x0;
    const mask1 = 1 << BIT2;
    a = a | mask1;
    printf("Contenido de a : %d \n", a);


    // Ejer 5: Declare una variable de 32 bits sin signo, con el valor inicial 0x00001234
    //		   y luego, mediante una operación de máscaras, invierta el bit 0.
    uint32_t b = 0x00001234;
    const uint32_t mask2 = 1 << BIT0;
    b = b ^ mask2;
    printf("Contenido de b: %d \n", b);


    // Ejer 6: Sobre una variable de 32 bits sin signo previamente declarada y de valor
    //		   desconocido asegúrese de colocar el bit 4 a 0 mediante máscaras y el operador <<
    uint32_t c;
    const uint32_t mask3 = 1 << BIT4;
    c = c & ~mask3;
    printf("Contenido de c: %d \n", c);

	return 0;
}

void dec2bin(int num) {
	unsigned int i;
	for (i = sizeof(num)*8-1; i>=0; --i) {
		if (num & (1 << i))
			printf("1");
		else
			printf("0");
	}
}

/*==================[end of file]============================================*/

