/*
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Bruno M. Breggia
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>
#include <stdlib.h>

/*==================[macros and definitions]=================================*/
#define BIT0 0
#define BIT1 1
#define BIT3 3
#define BIT4 4
#define BIT7 7

/*==================[internal functions declaration]=========================*/

int main(void)
{
	// Ejer 10: Sobre una variable de 16 bits previamente declarada,
	//         verifique si el bit 3 y el bit 1 son 1
    int16_t a;
    const int16_t mask1 = 1<<BIT1 | 1<<BIT3;
    if (a&mask1 == mask1) {
    	printf("Los bits 1 y 3 de 'a' valen 1\n");
    } else {
    	printf("Los bits 1 y 3 de 'a' NO valen 1\n");
    }
    printf("El contenido de 'a' es: %d \n\n", a);


    // Ejer 11: Sobre una variable de 8 bits previamente definida,
    //          verifique si los bits 0 y 7 son iguales
    int8_t b;
    // Se puede hacer sin mascaras... estaria bien?
    const int16_t mask2 = 1<<BIT0;
    const int16_t mask3 = 1<<BIT7;

    //Con mascaras
    if ( (b&mask2)>>BIT0 == (b&mask3)>>BIT7 ) {
    	printf("Los bits 0 y 7 de 'b' son iguales \n");
    } else {
    	printf("Los bits 0 y 7 de 'b' NO son iguales \n");
    }

    //Sin mascaras
    if ( (b>>BIT0)%2 == (b>>BIT7)%2 ) {
        	printf("Los bits 0 y 7 de 'b' son iguales \n");
        } else {
        	printf("Los bits 0 y 7 de 'b' NO son iguales \n");
        }

    printf("El contenido de 'b' es: %d \n\n", b);


    // Ejer 14: Declare un puntero a un entero con signo de 16 bits y cargue inicialmente
    //          el valor -1. Luego, mediante máscaras, coloque un 0 en el bit 4
    int16_t c, *ptr_c;
    // Trabajamos con memoria dinamica?
    const int16_t mask4 = 1 << BIT4;

    //Sin memoria dinamica
    c = -1;
    ptr_c = &c;
    *ptr_c = (*ptr_c) & ~mask4;
    printf("El contenido de 'c' es %d \n", c);

    //Con memoria dinamica
    ptr_c = malloc(sizeof(int16_t));
    *ptr_c = -1;
    *ptr_c = (*ptr_c) & ~mask4;
    printf("El contenido al que apunta 'ptr_c' es: %d \n\n", *ptr_c);
    free(ptr_c);


	return 0;
}


/*==================[end of file]============================================*/

