/*
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Bruno M. Breggia
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>

/*==================[macros and definitions]=================================*/
#define BIT3 3
#define BIT4 4
#define BIT6 6
#define BIT14 14

/*==================[internal functions declaration]=========================*/

int main(void)
{
	// Ejer 1: Declare una constante de 32 bits, con todos los bits en 0 y
	//		   el bit 6 en 1. Utilice el operador <<
	const int32_t a = 1 << BIT6;
	printf("Contenido de a: %d \n", a);


	// Ejer 2: Declare una constante de 16 bits, con todos los bits en 0 y
	//         los bits 3 y 4 en 1. Utilice el operador <<
	const int16_t b = (1 << BIT3) | (1 << BIT4);
	printf("Contenido de b: %d \n", b);


	// Ejer 3: Declare una variable de 16 bits sin signo, con el valor
	//         inicial 0xFFFF y luego, mediante una operación de máscaras, coloque a 0 el bit 14
	uint16_t c = 0xFFFF;
	const uint16_t mask = 1 << BIT14;
	c = c & ~mask;
	printf("Contenido de c: %d \n", c);

	return 0;
}

/*==================[end of file]============================================*/

