/*
 * Asignatura: Electrónica Programable
 * FIUNER - 2020
 * Autor/es: Bruno M. Breggia
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdint.h>

/*==================[macros and definitions]=================================*/
#define BIT3 3
#define BIT4 4
#define BIT5 5
#define BIT13 13
#define BIT14 14


/*==================[internal functions declaration]=========================*/

int main(void)
{
	// Ejer 7: Sobre una variable de 32 bits sin signo previamente declarada y
	//         de valor desconocido, asegúrese de colocar el bit 3 a 1 y los
	//         bits 13 y 14 a 0 mediante máscaras y el operador <<
    uint32_t a;
    const uint32_t mask1 = 1 << BIT3;
    const uint32_t mask2 = 1 << BIT13 | 1 << BIT14;
    a = (a | mask1) & ~mask2;
    printf("Contenido de a: %d \n", a);


    // Ejer 8: Sobre una variable de 16 bits previamente declarada y de valor desconocido,
    //         invierta el estado de los bits 3 y 5 mediante máscaras y el operador <<
	uint16_t b;
	const uint16_t mask3 = 1 << BIT3 | 1 << BIT5;
	b = b ^ mask3;
	printf("Contenido de b: %d \n", b);


	// Ejer 9: Sobre una constante de 32 bits previamente declarada, verifique si el bit 4 es 0.
	//     Si es 0, cargue una variable “A” previamente declarada en 0, si es 1, cargue “A” con 0xaa
	const int32_t c;
	int32_t A;
	const int32_t mask4 = 1 << BIT4;
	if ( !(c & mask4) ) {
		A = 0x00;
	} else {
		A = 0xaa;
	}

	return 0;
}

// No me funciona...
void dec2bin(int num) {
	unsigned int i;
	for (i = sizeof(num)*8-1; i>=0; --i) {
		if (num & (1 << i))
			printf("1");
		else
			printf("0");
	}
}

/*==================[end of file]============================================*/

